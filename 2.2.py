#Atribui um valor à variável cebolas
cebolas = 300

#Atribui um valor à variável cebolas_na_caixa
cebolas_na_caixa = 120

#Atribui um valor à variável espaco_caixa
espaco_caixa = 5

#Atribui um valor à variável caixa
caixas = 60

#Atribui um valor à variável cebolas_fora_da_caixa, pela subtração do valor de cebolas pelo valor de cebolas_na_caixa
cebolas_fora_da_caixa = (cebolas - cebolas_na_caixa)

#Atribui um valor à variável caixas_vazias, pela subtração do valor de caixa pela divisão cebolas_na_caixa por espaco_caixa
caixas_vazias = (caixas - (cebolas_na_caixa / espaco_caixa))

#Atribui um valor à caixa_necessarias, através da divisão de cebolas_fora_da_caixa por espaco_caixa
caixas_necessarias = (cebolas_fora_da_caixa / espaco_caixa)

#Imprime o valor de cebolas_na_caixa
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")

#Imprime o valor de cebolas_fora_da_caixa
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")

#Imprime o valor de cebolas espaco_caixa
print ("Em cada caixa cabem", espaco_caixa, "cebolas")

#Imprime o valor de cebolas caixas_vazias
print ("Ainda temos", caixas_vazias, "caixas vazias")

#Imprime o valor de cebolas caixas_necessarias
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
